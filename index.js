Vue.component("app-composant-un", {
  name: "app-composant-un",
  props: ["prop"],
  template: `<div>Je suis le composant : app-composant-un + {{ prop }}</div>`,
});

Vue.component("app-composant-deux", {
  name: "app-composant-deux",
  template: `<div>Je suis le composant : app-composant-deux</div>`,
});

Vue.component("app-composant-trois", {
  name: "app-composant-trois",
  template: `<div>Je suis le composant : app-composant-trois</div>`,
});

let vm = new Vue({
  el: "#app",
  data: {
    valeur_1: false,
    valeur_2: "test",
    valuer_3: "vuejs c'est de la bombe",
  },
});

console.log("Les données de l'application Vue 👇");
console.table({ ...vm.$data });
console.log("------------------");

console.log("La propriété sur le composant app-composant-un 👇");
vm.$children.forEach((child) => {
  if (child.$props !== undefined) {
    console.table({ ...child.$props });
  }
});
console.log("------------------");

console.log("L'élément html sur lequel l'instance Vue s'est greffée 👇🏻");
console.log(vm.$el);
console.log("------------------");

console.log("Le plus haut parent de l'instance Vue 👇🏻");
console.log(vm.$root.$el);
console.log("------------------");
