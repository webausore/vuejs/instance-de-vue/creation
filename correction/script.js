Vue.component('app-composant-un', {
	template: `
		<div>
			contenu composant un
			<p>Props : {{ test }}</p>
		</div>
	`,
	props: ['test'],
});

Vue.component('app-composant-deux', {
	template: `
		<div>
			contenu composant deux
		</div>
	`,
});

Vue.component('app-composant-trois', {
	template: `
		<div>
			contenu composant trois
		</div>
	`,
});

const app = new Vue({
	el: '#app',
	data: {
		valeur_1: false,
		valeur_2: 'test',
		valeur_3: "vuejs c'est de la bombe",
	},
});

console.log('app.$data 👇');
console.table({ ...app.$data });

console.log('---------------');

console.log('propriété composant un 👇');
app.$children.forEach((children) => {
	if (children.$props) console.table({ ...children.$props });
});

console.log('---------------');

console.log("élément html de l'instance 👇");
console.log(app.$el);

console.log('---------------');

console.log("plus haut élément html de l'instance 👇");
console.log(app.$root.$el);
