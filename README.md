# vuejs-instance-de-vue-creation

Preview (ouvrir la console) : https://vuejs-instance-de-vue-creation.netlify.app/

Exercice :

-   Créer une structure vue avec index.html / script.js
-   Créer l'instance de vue en initialisant un objet data avec trois valeurs :
    -   valeur_1 = false
    -   valeur_2 = 'test'
    -   valuer_3 = 'vuejs c'est de la bombe'
-   Créer trois composants :
    -   app-composant-un avec un contenu fictif
    -   app-composant-deux avec un contenu fictif
    -   app-composant-trois avec un contenu fictif
-   Créer une propriété (props) sur le composant app-composant-un
-   Faire remonter en front les informations suivantes sur la structure Vue que tu viens de créer :
    -   Les données
    -   La propriété sur le composant app-composant-un
    -   L'élément html sur lequel l'instance vue s'est greffée
    -   Le plus haut parent de l'instance vue

Pour réaliser cette exercice, deux ressources :

-   Documentation : https://fr.vuejs.org/v2/guide/instance.html
-   API : https://fr.vuejs.org/v2/api/#Proprietes-d%E2%80%99instance
-   Si tu veux console log des tableaux comme je l'ai fais : https://developer.mozilla.org/fr/docs/Web/API/Console/table
